from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Category'))
    # slug = models.SlugField(max-length=80, unique=True)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __str__(self):
        return self.name


class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', _('Draft')),
        ('published', _('Published')),
    )

    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name="posts")
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name=_('Title'))
    text = models.TextField(verbose_name=_('Text'))
    image = models.FileField(verbose_name=_('Post image'), upload_to='post_image/')
    slug = models.SlugField(max_length=80, db_index=True)
    pub_date = models.DateField(verbose_name=_('Published date'), auto_now_add=False)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='draft', verbose_name=_('Status'))

    class Meta:
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')
        get_latest_by = ['-priority']

    def __str__(self):
        return self.name


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="comments")
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name="comments")
    text = models.CharField(max_length=255, verbose_name=_('Comment'))
    create_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Comment')
        verbose_name_plural = _('Comments')
        get_latest_by = ['-priority']

    def __str__(self):
        return "%s-%s" % (self.author, self.create_date)