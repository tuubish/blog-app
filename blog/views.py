from django.shortcuts import render
from django.views.generic import TemplateView, ListView

from blog.models import Category, Post


class CategoryListView(TemplateView):
    template_name = "navbar.html"

    def get_context_data(self, **kwargs):
        categories = Category.objects.all()
        return {
            'categories': categories
        }


class HomePageView(TemplateView):
    template_name = "blog/home.html"

    def get_context_data(self, **kwargs):
        posts = Post.objects.all()
        return {
            'posts': posts
        }


class PostDetailView(TemplateView):
    template_name = 'post_detail.html'

    def dispatch(self, request, *args, **kwargs):
        post_id = kwargs["post_id"]
        post = Post.object.get(id=post_id)
        return {
            'post': post
        }