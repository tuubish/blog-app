from django.contrib import admin

# Register your models here.
from blog.models import Category, Post, Comment


class PostModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'author', 'pub_date', 'status']
    search_fields = ['name', 'author', 'pub_date']


admin.site.register(Category)
admin.site.register(Post, PostModelAdmin)
admin.site.register(Comment)
