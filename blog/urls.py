from django.urls import path

from blog.views import HomePageView, CategoryListView

urlpatterns = [
    path('', HomePageView.as_view(), name='home_page'),
    path('category/<str:name>', CategoryListView.as_view(), name='category_list')
]